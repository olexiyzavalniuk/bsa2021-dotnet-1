﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa2021_dotnet_1.DTO
{
    class Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Models.TaskState State { get; set; }
        public string CreatedAt { get; set; }
        public string FinishedAt { get; set; }
    }
}
