﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa2021_dotnet_1.DTO
{
    class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedAt { get; set; }
    }
}
