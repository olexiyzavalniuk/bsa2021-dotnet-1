﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Linq;
using System.Threading.Tasks;

namespace bsa2021_dotnet_1
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var projects = await Get.Projects();
            var tasks = await Get.Tasks();
            var teams = await Get.Teams();
            var users = await Get.Users();

            var hierarchies = projects.GroupJoin(tasks, project => project.Id,
                task => task.ProjectId, (project, tasks) =>
            new Models.Hierarchy
            {
                Id = project.Id,
                Name = project.Name,
                Description = project.Description,
                Deadline = project.Deadline,
                Tasks = tasks.Where(task => task.ProjectId == project.Id)
                .Join(users, task => task.PerformerId, user => user.Id, (task, user) =>
                    new Models.Task
                    {
                        Id = task.Id,
                        Name = task.Name,
                        Description = task.Description,
                        State = task.State,
                        CreatedAt = task.CreatedAt,
                        FinishedAt = task.FinishedAt,
                        Performer = users.First(user => user.Id == task.PerformerId)
                    }).ToList(),
                Team = teams.First(team => team.Id == project.TeamId),
                Author = users.First(user => user.Id == project.AuthorId)
            }).ToList();

            Console.WriteLine(" This is console interface. You can use command ");
            Console.WriteLine(" \"man\" to see list of available commands. And ");
            Console.WriteLine(" \"quit\" to exit from program ");
            Console.WriteLine();

            bool flag = true;
            while (flag)
            {
                Console.Write(">");
                string command = Console.ReadLine();
                Console.WriteLine();
                switch (command)
                {
                    case "quit":
                        flag = false;
                        break;
                    case "one":
                        One(hierarchies);
                        break;
                    case "two":
                        Two(hierarchies);
                        break;
                    case "three":
                        Three(hierarchies);
                        break;
                    //case "seven":
                    //    Seven(hierarchies);
                    //    break;
                    case "man":
                        Man();
                        break;
                    default:
                        Console.WriteLine("What? =/");
                        break;
                }
                Console.WriteLine();
            }
        }

        static void Man()
        {
            Console.WriteLine("one - first method");
            Console.WriteLine("two - second method");
            Console.WriteLine("three - third method");
        }

        static void One(List<Models.Hierarchy> hierarchies)
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = First(hierarchies, id);
                foreach (var item in result)
                {
                    Console.WriteLine("Project: " + item.Key.Name);
                    Console.WriteLine("Task Count: " + item.Value);
                }  
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        static void Two(List<Models.Hierarchy> hierarchies)
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = Second(hierarchies, id);
                foreach (var item in result)
                {
                    Console.WriteLine(item.Name);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        static void Three(List<Models.Hierarchy> hierarchies)
        {
            Console.Write("Enter Id: ");
            try
            {
                int id = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                var result = Third(hierarchies, id);
                foreach (var item in result)
                {
                    Console.WriteLine(item.id + " " + item.name);
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Error");
            }
        }

        //static void Seven(List<Models.Hierarchy> hierarchies)
        //{
        //    try
        //    {
        //        var result = Seventh(hierarchies);
        //        foreach (var item in result)
        //        {
        //            Console.WriteLine(item.Item1.Name);
        //            Console.WriteLine(item.Item2.Name);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        Console.WriteLine("Error");
        //    }
        //}

        static Dictionary<Models.Hierarchy, int> First(List<Models.Hierarchy> hierarchies, int id)
        {
            Dictionary<Models.Hierarchy, int> result;
            result = hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => task.Performer.Id == id)).
                ToDictionary(
                h => h,
                h => h.Tasks.Where(task => task.Performer.Id == id).Count()
                );

            return result;
        }

        static List<Models.Task> Second(List<Models.Hierarchy> hierarchies, int id)
        {
            List<Models.Task> result;
            result = hierarchies.Where(hierarchy => hierarchy.Tasks.Any(task => task.Performer.Id == id &&
            task.Name.Length < 45)).
                SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id)).ToList();

            return result;
        }

        static List<Models.Third> Third(List<Models.Hierarchy> hierarchies, int id)
        {
            List<Models.Third> result;
            result = hierarchies
               .SelectMany(h => h.Tasks.Where(t => t.Performer.Id == id && t.FinishedAt != null 
               && t.FinishedAt.Contains("2021")))
               .Select(t => new Models.Third()
               {
                   id = t.Id,
                   name = t.Name
               }).ToList();

            return result;
        }

        //static IEnumerable<(Models.Hierarchy, Models.Task, Models.Task, IEnumerable<DTO.User>)> Seventh(List<Models.Hierarchy> hierarchies)
        //{
        //    var result = hierarchies.Select(h =>
        //    (h,
        //    h.Tasks != null ? h.Tasks.Aggregate((curMax, t) => t.Description.Length < curMax.Description.Length ? t : curMax) : null,
        //    h.Tasks != null ? h.Tasks.Aggregate((curMin, t) => t.Name.Length < curMin.Name.Length ? t : curMin) : null,
        //    h.Description.Length > 20 || h.Tasks.Count < 3 ? h.Tasks.Select(t => t.Performer).Distinct() : null));

        //    return result;
        //}

        //static List<Models.Fifth> Fifth(List<Models.Hierarchy> hierarchies, DTO.Task[] tasks)
        //{
        //    List<Models.Fifth> result;
        //    result = hierarchies.SelectMany(h => h.Tasks.Select(t => t.Performer))
        //        .Join(tasks, u => u.Id, t => t.PerformerId, (u, t) =>
        //        new Models.Fifth
        //        {
        //            user = t.PerformerId,
        //            tasks
        //        });

        //    return result;
        //}

    }


}
