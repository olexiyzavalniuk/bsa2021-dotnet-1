﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Json;
using bsa2021_dotnet_1.DTO;

namespace bsa2021_dotnet_1
{
    static class Get
    {
        private const string _path = "https://bsa21.azurewebsites.net/api/";
        private static HttpClient _client = new HttpClient();

        public static Task<Project[]> Projects()
        {
            return _client.GetFromJsonAsync<Project[]>(_path + "Projects");
        }

        public static Task<Project> Project(int id)
        {
            return _client.GetFromJsonAsync<Project>(_path + "Projects/" + id);
        }

        public static Task<DTO.Task[]> Tasks()
        {
            return _client.GetFromJsonAsync<DTO.Task[]>(_path + "Tasks");
        }

        public static Task<DTO.Task> Task(int id)
        {
            return _client.GetFromJsonAsync<DTO.Task>(_path + "Tasks/" + id);
        }

        public static Task<Team[]> Teams()
        {
            return _client.GetFromJsonAsync<Team[]>(_path + "Teams");
        }

        public static Task<Team> Team(int id)
        {
            return _client.GetFromJsonAsync<Team>(_path + "Teams/" + id);
        }
        public static Task<User[]> Users()
        {
            return _client.GetFromJsonAsync<User[]>(_path + "Users");
        }

        public static Task<User> User(int id)
        {
            return _client.GetFromJsonAsync<User>(_path + "Users/" + id);
        }
    }
}
