﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bsa2021_dotnet_1.DTO;

namespace bsa2021_dotnet_1.Models
{
    class Task
    {
        public int Id { get; set; }
        public User Performer { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
        public string CreatedAt { get; set; }
        public string FinishedAt { get; set; }
    }
}
