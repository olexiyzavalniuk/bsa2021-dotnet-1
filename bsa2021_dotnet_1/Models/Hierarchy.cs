﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bsa2021_dotnet_1.DTO;

namespace bsa2021_dotnet_1.Models
{
    class Hierarchy
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
        public List<Task> Tasks { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Deadline { get; set; }
    }
}
