﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa2021_dotnet_1.Models
{
    public enum TaskState
    {
        ToDo = 1,
        InProgress,
        Done,
        Canceled
    }
}
